require "http/client"

HTTP::Client.get("https://konstantinev.com") do |response|
  response.status_code  # => 200
  puts response.body # => "<!doctype html>"
end
