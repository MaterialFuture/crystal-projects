# First Attempt
def remove_old(s)
  s = s.split("")
  s[-1] === "!" ? (s.first s.size - 1).join : s.join
end

def remove(s)
  s.chomp("!")
end
# This was me a case of me forgetting chomp, but good that I know how to do it otherwise.

# Tests
puts remove("!me!!!")
puts remove("!me")
puts remove("!me!")
puts remove("hi")
puts remove("hi!")

# Tested, Works
