def first_non_consecutive(arr)
  arr.each_cons(2){|x| x[0]+1 == x[1] ? nil : return x[1]}
  return nil
end
# I'm still confused as to why it needed the last nil to work, but that's what it required,
# otherwise it would have been a one-liner

# Tests
puts first_non_consecutive([1,2,5,6,8])
puts first_non_consecutive([1,3,4,5,6])

# Tested, Works
