# First Pass
def litres_old(t)
  (t*0.5).floor.to_i
end
# Floor is important in controlling how floats are handled

def litres(t)
  (t*0.5).to_i
end
# Depending on what is the purpose, cutting off the floating point values will result in the
# correct answer for the kata, but in general this will not be the case.
# In a real world scenario, you wouldn't just shave off the numbers, that could lead to some issues later on.

# To round up you simply use x.roud(n) for rounding to whatever value.

# Tests
puts litres(3)
puts litres(6.7)
puts litres(5)

# Tests Pass
