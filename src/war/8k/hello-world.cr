# Please Note that this is a bad example of how to do a hello world
# It's intentionally bad. The point of the kata was to be creative in how you do hello world.
class HelloWorld
  macro hw(value)
    helloworld.push({{value}})
  end

  def helloworld_fin()
    helloworld = Array(Char).new(12) # Create Array of Chars
    c_arr : Array(Char) = [ # List out all char escape sequences
      ' ',      # space
      '\u0064', # d
      '\u0065', # e
      '\u0068', # h
      '\u006C', # l
      '\u006F', # o
      '\u0072', # r
      '\u0077', # w
      '\u0021', # !
    ]
    # I'm not sure if there would be a benefit to using escape sequences in something like this.

    # Create those Arrays
    hw(c_arr[3])
    hw(c_arr[2])
    hw(c_arr[4])
    hw(c_arr[4])
    hw(c_arr[5])
    hw(c_arr[0])
    hw(c_arr[7])
    hw(c_arr[5])
    hw(c_arr[6])
    hw(c_arr[4])
    hw(c_arr[1])
    hw(c_arr[8])

    return helloworld
  end
end

def greet()
  return HelloWorld.new.helloworld_fin().join("").to_s.downcase
end

# Test
puts greet()

# Works, tested


# Bonus: interesting one...
"‮"; def greet() "hello world!" end
puts greet()
