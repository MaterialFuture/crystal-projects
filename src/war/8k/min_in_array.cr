# First attempt
def find_smallest_int_old(arr)
  (arr.sort)[0]
end

# Refactor
def find_smallest_int(a)
  a.min
end
# Another case of forgetting an attribute exists.
# It does the same as above, but of course is cleaner.

# Test
puts find_smallest_int([55,-32,41,5])

# Passes, Works
