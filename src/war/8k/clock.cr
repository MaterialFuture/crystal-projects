# First Pass
def past_old(h, m, s)
  ((h*60**2)*10**3)+((m*60)*10**3)+((s*10**3))
end
# This approach was thinking more so how it could be modular, say if more arguments were added

# Refactor
def past(h, m, s)
  (((h*60+m)*60)+s)*1000
end
# This is simply moving things around to be more efficient

# Tests
puts past_old(1,2,2)
puts past(1,2,2)

# Tests Passed, Works
