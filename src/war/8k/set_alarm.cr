# First Attempt
def set_alarm_old(e, v)
  e == true && v == false ? true : false
end

# Refactor
def set_alarm(e, v)
  e && !v
end
# setting a return value of true or false is redundant
# Also, testing if a value is not something is like on other langs with !x

# Tests
puts set_alarm(true, false)
puts set_alarm(true, true)
puts set_alarm(false, false)
