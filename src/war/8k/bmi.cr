# First Pass
def bmi_old(weight, height)
  bmi = weight/(height*height)
  if bmi <= 18.5
    return "Underweight"
  elsif bmi <= 25.0
    return "Normal"
  elsif bmi <= 30.0
    return "Overweight"
  elsif bmi > 30
    return "Obese"
  else nil
  end
end

# Refactor
def bmi(weight, height)
  b = weight / height ** 2
  case
    when b <= 18.5 then "Underweight"
    when b <= 25.0 then "Normal"
    when b <= 30.0 then "Overweight"
    else "Obese"
  end
end
# This is a good case on when not to use if/else, you can see which is easier to read
# Cases have their own syntax for return values called then, which is easier to read

# Tests
puts bmi(50, 1.80)
puts bmi(90, 1.50)

# Tested, Works
