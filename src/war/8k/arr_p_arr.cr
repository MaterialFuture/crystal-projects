# Initial
def array_plus_array_old(arr1, arr2)
  return (arr1+arr2).sum
end

# Refactor

def array_plus_array(a, b)
  (a+b).sum
end
# Return is redundant

# Test
puts array_plus_array([1,2,3],[4,5,6])

# Tested, works
