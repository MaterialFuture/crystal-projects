# First pass
def odd_count(n : Int)
  x = 0
  (1...n).each do |i|
    if i.odd? == true
      x = x + 1
    end
  end
  return x
end
# Clearly over-complicated the problem.

# Refactor
def odd_count(n : Int)
  n/2
end

# Tests
odd_count(2)
odd_count(200)
odd_count(34534536543654)
odd_count(4358294578329)
